package damo.cs.upc.edu.convertidormkm;

/**
 * Interfície patatera; lísteners per codi
 */

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class ConvertidorMKM extends Activity {
    public static final double FACTOR_DE_CONVERSIO = 1.609344;
    private RadioButton radioKm;
    private RadioButton radioMilles;
    private EditText text;
    private Button botoConversio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor_mkm);
        inicialitza();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.convertidor_mkm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void inicialitza() {
        text = findViewById(R.id.editText);
        radioKm =  findViewById(R.id.radioButtonKm);
        radioMilles =  findViewById(R.id.radioButtonMilles);
        botoConversio =  findViewById(R.id.buttonConvert);

         text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                esborrar(view);
            }
        });

        botoConversio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conversio(v);
            }
        });
    }



    private void conversio(View view) {
       float inputValue;

        try {
            inputValue = Float.parseFloat(text.getText().toString());
        }
        catch (Exception e){
            return;
        };

        if (radioKm.isChecked()) {
            text.setText(String.valueOf(inputValue / FACTOR_DE_CONVERSIO));
            radioKm.setChecked(false);
            radioMilles.setChecked(true);
        }
        else {
            text.setText(String.valueOf(inputValue * FACTOR_DE_CONVERSIO));
            radioMilles.setChecked(false);
            radioKm.setChecked(true);
        }
}

    private void esborrar(View v){
        ((TextView) v).setText("");
    }

}
